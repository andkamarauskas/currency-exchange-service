# Currency Exchange Api

---

## Requirements

-   node v10.21.0
-   yarn v1.17.3

## Install

    $ git clone https://gitlab.com/andkamarauskas/currency-exchange-service
    $ cd currency-exchange-service
    $ yarn install

## Configure app

Change `.env.example` to `.env` and configure if needed.
Open `src/config/index.js` then edit it with your settings.

## Running the project

    $ yarn start

## Swagger API documentation

`http://127.0.0.1:2323/api-docs`
