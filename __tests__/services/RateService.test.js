const axios = require('axios');

const RateService = require('../../src/services/RateService');

jest.mock('axios');

describe('Rate service test', () => {
	it('should return exchange rate and calculated quote amount', async () => {
		const response = {
			data: {
				rates: { USD: 1.124, ILS: 4.0042, GBP: 0.91213 },
				base: 'EUR',
				date: '2020-09-09',
			},
		};
		axios.get.mockResolvedValue(response);

		const {
			exchange_rate,
			quote_amount,
		} = await RateService.calculateQuoteAmount({
			base_currency: 'EUR',
			quote_currency: 'USD',
			base_amount: 100,
		});

		expect(exchange_rate).toEqual(1.124);
		expect(quote_amount).toEqual(112);
	});
});
