module.exports = {
	handleResponseError,
};

function handleResponseError(res, e) {
	if (e.params_validation) return res.status(400).send({ errors: e.errors });

	if (e.result_validation) {
		e.endpoint = res.req.originalUrl;
		e.endpoint_params = {
			body: res.req.body,
			params: res.req.params,
			query: res.req.query,
		};
		console.error(
			`Response result validation error`,
			JSON.stringify(e, null, 2),
		);
	} else {
		console.error(`Unhandle response error`, e);
	}
	return res.status(500).send(`Internal service error`);
}
