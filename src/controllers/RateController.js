module.exports = { getQouteAmountAndExchangeRate };

const {
	validateQuoteAmountQueryParams,
	validateQuoteAmountResult,
} = require('../validators/QuoteAmountEnpointValidator');

const { handleResponseError } = require('../helpers/ResponseErrorHandler');
const { calculateQuoteAmount } = require('../services/RateService');

async function getQouteAmountAndExchangeRate(req, res) {
	try {
		const query_params = validateQuoteAmountQueryParams(req.query);

		const result = await calculateQuoteAmount(query_params);

		const { exchange_rate, quote_amount } = validateQuoteAmountResult(
			result,
		);

		return res.send({ exchange_rate, quote_amount });
	} catch (error) {
		return handleResponseError(res, error);
	}
}
