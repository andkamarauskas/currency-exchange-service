const development = {
	exchange_rates: {
		SUPPORTED_CURRENCIES: ['EUR', 'USD', 'GBP', 'ILS'],
		EXHCANGE_RATES_API_HOST: 'https://api.exchangeratesapi.io',
		RATE_CACHE_CAPACITY: 2,
		RATE_CACHE_MAX_AGE_MS: 10000,
	},
};

const testing = {
	exchange_rates: {
		SUPPORTED_CURRENCIES: ['EUR', 'USD', 'GBP', 'ILS'],
		EXHCANGE_RATES_API_HOST: 'https://api.exchangeratesapi.io',
		RATE_CACHE_CAPACITY: 2,
		RATE_CACHE_MAX_AGE_MS: 10000,
	},
};

const production = {
	exchange_rates: {
		SUPPORTED_CURRENCIES: ['EUR', 'USD', 'GBP', 'ILS'],
		EXHCANGE_RATES_API_HOST: 'https://api.exchangeratesapi.io',
		RATE_CACHE_CAPACITY: 2,
		RATE_CACHE_MAX_AGE_MS: 5 * 60 * 1000,
	},
};

switch (process.env.NODE_ENV || 'development') {
	case 'development':
		module.exports = development;
		break;
	case 'testing':
		module.exports = testing;
		break;
	case 'production':
		module.exports = production;
		break;

	default:
		throw new Error(
			"Please specify environment variable 'NODE_ENV' out of {'development', 'testing', 'production'}.",
		);
}
