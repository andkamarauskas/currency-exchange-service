module.exports = { getBaseCurrencyRates, getCachedRate };

const axios = require('axios');

const LRU = require('lru-cache');

const {
	SUPPORTED_CURRENCIES,
	RATE_CACHE_CAPACITY,
	RATE_CACHE_MAX_AGE_MS,
	EXHCANGE_RATES_API_HOST,
} = require('../../config').exchange_rates;

const ratesCache = new LRU({
	max: RATE_CACHE_CAPACITY,
	maxAge: RATE_CACHE_MAX_AGE_MS,
});

/**
 * Returns base currency rates
 *
 * @param {string} base_currency String, 3 letters ISO currency code.
 *
 * @returns {Object.<string, number>}
 */
function getBaseCurrencyRates(base_currency) {
	return axios
		.get(`${EXHCANGE_RATES_API_HOST}/latest?base=${base_currency}`)
		.then(res => res.data.rates)
		.catch(e => {
			const error = new Error(`Error getting base currency`);
			e = e.response || e;
			error.errors = e.data || e;
			throw error;
		});
}

/**
 * Method returns base and quote currency pair exchange rate
 *
 * @param {string} base_currency String, 3 letters ISO currency code.
 * @param {string} quote_currency String, 3 letters ISO currency code.
 *
 * @returns {number}
 */
async function getCachedRate(base_currency, quote_currency) {
	let rates = ratesCache.get(base_currency);

	if (!rates) {
		rates = await loadBaseCurrencyRatesToCache(base_currency);
	}

	const rate = rates.find(rate => rate.currency == quote_currency);

	if (!rate) {
		throw new Error(`Cannot get ${quote_currency} currency rate`);
	}

	return rate.value;
}

/**
 *
 * Method loads base currency rates to cache. Only supported currencies will be loaded.
 *
 * @param {stirng} base_currency String, 3 letters ISO currency code.
 *
 * @returns {Object[]}
 */
async function loadBaseCurrencyRatesToCache(base_currency) {
	const currenciesToLoad = SUPPORTED_CURRENCIES.filter(
		c => c != base_currency,
	);

	const totalRates = await getBaseCurrencyRates(base_currency);

	const rates = currenciesToLoad.map(currency => {
		return { currency, value: totalRates[currency] };
	});

	ratesCache.set(base_currency, rates);

	return rates;
}
