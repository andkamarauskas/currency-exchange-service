module.exports = {
	calculateQuoteAmount,
};

const BN = require('bignumber.js');

const { getCachedRate } = require('./thirdPartyProviders/ExchangeRatesApi');

/**
 * @typedef {Object} RateAndQuoteAmount
 * @property {number} exchange_rate - Decimal, the offered exchange rate. Up to 3 decimal digits.
 * @property {number} quote_amount - Integer, the expected amount in cents.
 */

/**
 * Method returns exchange rate and calculated quote
 *
 * @param {Object} query_params
 * @param {string} query_params.quote_currency String, 3 letters ISO currency code. Currency to convert from.
 * @param {string} query_params.base_currency String, 3 letters ISO currency code. Currency to convert to.
 * @param {number} query_params.base_amount Integer. The amount to convert in cents. Example: 100 (1 USD)
 *
 * @returns {RateAndQuoteAmount}
 */
async function calculateQuoteAmount(query_params) {
	const { base_currency, quote_currency, base_amount } = query_params;

	const rate = await getCachedRate(base_currency, quote_currency);

	const exchange_rate = new BN(rate).toFixed(3, 1);

	const quote_amount = new BN(base_amount).times(exchange_rate).toFixed(0, 1);

	return {
		exchange_rate: Number(exchange_rate),
		quote_amount: Number(quote_amount),
	};
}
