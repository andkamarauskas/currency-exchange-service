require('dotenv').config();
const swaggerUi = require('swagger-ui-express');
const { connector } = require('swagger-routes-express');
const fs = require('fs');
const path = require('path');
const jsyaml = require('js-yaml');

const express = require('express');
const app = express();

const spec = fs.readFileSync(
	path.join(__dirname + '/swagger/swagger.yaml'),
	'utf8',
);

const swaggerDocument = jsyaml.safeLoad(spec);

const port = process.env.PORT || 2323;

const api = require('./controllers');

const connect = connector(api, swaggerDocument);

connect(app);

app.use(
	'/api-docs',
	swaggerUi.serve,
	swaggerUi.setup(swaggerDocument, {
		explorer: true,
	}),
);

app.listen(port);
console.log(`Listening on port ${port}`);
