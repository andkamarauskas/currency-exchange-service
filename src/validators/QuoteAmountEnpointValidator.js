module.exports = {
	validateQuoteAmountQueryParams,
	validateQuoteAmountResult,
};

const Joi = require('joi');

const { SUPPORTED_CURRENCIES } = require('../config').exchange_rates;

const quoteAmountParamsSchema = Joi.object({
	quote_currency: Joi.string()
		.uppercase()
		.min(3)
		.max(3)
		.valid(...SUPPORTED_CURRENCIES)
		.required(),
	base_currency: Joi.string()
		.uppercase()
		.min(3)
		.max(3)
		.valid(...SUPPORTED_CURRENCIES)
		.required(),
	base_amount: Joi.number()
		.integer()
		.min(1)
		.required(),
});

function validateQuoteAmountQueryParams(query_params) {
	const { error, value } = quoteAmountParamsSchema.validate(query_params);

	if (error) {
		const parsedError = {
			params_validation: true,
			errors: error.details.map(i => i.message),
		};
		throw parsedError;
	}

	return value;
}

const quoteAmountResultSchema = Joi.object({
	exchange_rate: Joi.number()
		.precision(3)
		.min(0.001)
		.required(),
	quote_amount: Joi.number()
		.integer()
		.min(0)
		.required(),
});

function validateQuoteAmountResult(query_params) {
	const { error, value } = quoteAmountResultSchema.validate(query_params);

	if (error) {
		const parsedError = {
			result_validation: true,
			errors: error.details,
		};
		throw parsedError;
	}

	return value;
}
